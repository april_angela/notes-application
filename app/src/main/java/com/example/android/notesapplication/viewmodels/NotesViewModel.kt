package com.example.android.notesapplication.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.android.notesapplication.db.Notes
import com.example.android.notesapplication.repo.NotesRepository

class NotesViewModel(application: Application) : AndroidViewModel(application) {
    private var mRepository = NotesRepository(application)

    fun getAllNotes(): LiveData<List<Notes>> {
        return mRepository.getAllNotes()
    }

    fun getFavNotes(): LiveData<List<Notes>> {
        return mRepository.getFavNotes()
    }

    fun insertNotes(notes: Notes) {
        mRepository.insertNotes(notes)
    }

    fun updateNotes(notes: Notes) {
        mRepository.updateNotes(notes)
    }


}