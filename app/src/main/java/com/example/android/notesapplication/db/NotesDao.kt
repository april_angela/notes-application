package com.example.android.notesapplication.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface NotesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNotes(notes: Notes)

    @Query("SELECT * FROM notes")
    fun getAllNotes() : LiveData<List<Notes>>

    @Query("SELECT * FROM notes WHERE isFavourite = 1")
    fun getFavNotes(): LiveData<List<Notes>>

    @Update
    fun updateNotes(notes: Notes)

}