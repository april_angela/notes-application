package com.example.android.notesapplication.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.notesapplication.adapter.NotesAdapter
import com.example.android.notesapplication.viewmodels.NotesViewModel
import com.example.android.notesapplication.R

class AllNotesFragment : Fragment() {

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mViewModel: NotesViewModel
    private lateinit var mAdapter: NotesAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_notes, null)

        mAdapter = NotesAdapter()
        mViewModel = (activity as MainActivity).getViewModel()
        mViewModel.getAllNotes().observe(viewLifecycleOwner, Observer {
            mAdapter.setNotes(it)
        })

        mAdapter.setViewModel(mViewModel)

        mRecyclerView = view.findViewById(R.id.recyclerViewAllNotes)
        mRecyclerView.apply {
            layoutManager = LinearLayoutManager(view.context)
            adapter = mAdapter
            isNestedScrollingEnabled = false
        }

        return view
    }
}