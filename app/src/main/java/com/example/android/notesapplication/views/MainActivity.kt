package com.example.android.notesapplication.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.android.notesapplication.db.Notes
import com.example.android.notesapplication.viewmodels.NotesViewModel
import com.example.android.notesapplication.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    companion object {
        const val NEW_NOTES_ACTIVITY_REQUEST_CODE = 1;
    }

    private lateinit var mToolbar: Toolbar
    private lateinit var mBottomNavigationView: BottomNavigationView
    private lateinit var mNavListener: BottomNavigationView.OnNavigationItemSelectedListener
    private lateinit var mNotesViewModel: NotesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //toolbar
        mToolbar = findViewById(R.id.toolBar)
        setSupportActionBar(mToolbar)
        mToolbar.inflateMenu(R.menu.top_menu)

        //load default fragment
        loadFragment(AllNotesFragment())

        //bottom navigation
        mBottomNavigationView = findViewById(R.id.bottom_navigation)
        mNavListener = BottomNavigationView.OnNavigationItemSelectedListener {
            lateinit var selectedFragment: Fragment
            when(it.itemId) {
                R.id.allNotes -> {
                    selectedFragment =
                        AllNotesFragment()
                }
                R.id.favourites -> {
                    selectedFragment =
                        FavouriteNotesFragment()
                }
            }
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, selectedFragment).
                commit()

            return@OnNavigationItemSelectedListener true
        }

        mBottomNavigationView.setOnNavigationItemSelectedListener(mNavListener)

        //viewModel
        mNotesViewModel = ViewModelProvider(this).get(NotesViewModel::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.top_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.addNotes -> {
                val intent = Intent(this, NotesActivity::class.java)
                startActivityForResult(intent,
                    NEW_NOTES_ACTIVITY_REQUEST_CODE
                )
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        var message: String
        var notesTitle: String
        var notesDesc: String
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == NEW_NOTES_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            notesTitle = data!!.getStringExtra(NotesActivity.EXTRA_NOTES_TITLE)
            notesDesc = data!!.getStringExtra(NotesActivity.EXTRA_NOTES_DESC)
            mNotesViewModel.insertNotes(
                Notes(
                    notesTitle,
                    notesDesc
                )
            )
            message = getString(R.string.message_notes_is_saved_successfully)

        } else {
            message = getString(R.string.message_notes_is_not_saved)
        }

        Toast.makeText(
            applicationContext,
            message,
            Toast.LENGTH_LONG
        ).show()
    }

    private fun loadFragment(fragment: Fragment) {
        if (fragment != null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()
        }
    }

    fun getViewModel(): NotesViewModel {
        return mNotesViewModel
    }

}