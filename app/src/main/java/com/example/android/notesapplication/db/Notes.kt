package com.example.android.notesapplication.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "notes")
data class Notes(
    @field:ColumnInfo(name="title")
    val title: String = "",

    @field:ColumnInfo(name="description")
    var description: String = "",

    @field:ColumnInfo(name="isFavourite")
    var isFavourite: Boolean = false,

    @field:ColumnInfo(name="createTime")
    var createTime: Long = Date().time,

    @field:PrimaryKey(autoGenerate = true)
    val id: Int = 0
)
