package com.example.android.notesapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.android.notesapplication.helper.DateHelper
import com.example.android.notesapplication.R
import com.example.android.notesapplication.db.Notes
import com.example.android.notesapplication.viewmodels.NotesViewModel
import kotlinx.android.synthetic.main.recyclerview_notes.view.*
import java.util.HashSet

class NotesAdapter : RecyclerView.Adapter<NotesAdapter.NotesViewHolder>() {
    private var mNotes: List<Notes> = ArrayList()
    private lateinit var mUniqueDates: HashSet<String>

    private lateinit var mViewModel: NotesViewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
       val itemView = LayoutInflater.from(parent.context).inflate(
           R.layout.recyclerview_notes,
           parent,
           false)

        return NotesViewHolder(
            itemView
        )
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        val currentItem = mNotes[position]
        val date = DateHelper.convertMilliSecondsToDate(
                currentItem.createTime,
                DateHelper.DATE_FORMAT
            )

        if (mUniqueDates.contains(date)) {
            holder.mNotesDateTv.visibility = View.GONE
        } else {
            holder.mNotesDateTv.visibility = View.VISIBLE
            holder.mNotesDateTv.text = date
            mUniqueDates.add(date)
        }

        holder.mNotesTitleTv.text = currentItem.title
        holder.mNotesDescTv.text = currentItem.description

        setFaveIcon(currentItem.isFavourite, holder.mNotesFavIv)

        holder.mNotesFavIv.setOnClickListener {
            currentItem.isFavourite = !currentItem.isFavourite

            mViewModel.updateNotes(currentItem)
        }

    }

    override fun getItemCount(): Int {
        return mNotes.size
    }

   private fun setFaveIcon(isFave: Boolean, imageView: ImageView) {
        if (isFave) {
            imageView.setImageResource(android.R.drawable.btn_star_big_on)
        } else {
            imageView.setImageResource(android.R.drawable.btn_star_big_off)
        }
    }

    fun setNotes(notes: List<Notes>) {
        mUniqueDates = HashSet()
        mNotes = notes
        notifyDataSetChanged()
    }

    fun setViewModel(viewModel: NotesViewModel) {
        mViewModel = viewModel
    }

    class NotesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mNotesDateTv: TextView = itemView.rvNotesDateTv
        val mNotesTitleTv: TextView = itemView.rvNotesTitleTv
        val mNotesDescTv: TextView = itemView.rvNotesDescTv
        val mNotesFavIv: ImageView = itemView.rvNotesFavIv
    }


}
