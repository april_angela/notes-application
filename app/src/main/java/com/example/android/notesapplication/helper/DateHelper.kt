package com.example.android.notesapplication.helper

import java.text.SimpleDateFormat
import java.util.Calendar

class DateHelper {
    companion object {
        const val DATE_FORMAT = "dd MMM yyyy"

        fun convertMilliSecondsToDate(milliSeconds: Long, dateFormat: String): String {
            val formatter = SimpleDateFormat(dateFormat)

            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            return formatter.format(calendar.time)
        }


    }
}