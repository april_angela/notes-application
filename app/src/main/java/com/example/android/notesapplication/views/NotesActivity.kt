package com.example.android.notesapplication.views

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.android.notesapplication.R
import kotlinx.android.synthetic.main.activity_notes.*

class NotesActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_NOTES_TITLE = "extra_notes_title"
        const val EXTRA_NOTES_DESC = "extra_notes_desc"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)

        val createBtn = findViewById<Button>(R.id.createBtn)
        createBtn.setOnClickListener {
            val replyIntent = Intent()

            val notesTitle = notesTitleTv.text.toString()
            val notesDesc = notesDescTv.text.toString()

            if(!TextUtils.isEmpty(notesTitle) && !TextUtils.isEmpty(notesDesc)) {
                replyIntent.putExtra(EXTRA_NOTES_TITLE, notesTitle)
                replyIntent.putExtra(EXTRA_NOTES_DESC, notesDesc)
                setResult(RESULT_OK, replyIntent)
            } else {
                setResult(RESULT_CANCELED, replyIntent)
            }

            finish()
        }

    }

}