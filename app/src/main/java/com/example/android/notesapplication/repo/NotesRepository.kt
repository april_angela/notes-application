package com.example.android.notesapplication.repo

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.android.notesapplication.db.AppDatabase
import com.example.android.notesapplication.db.Notes
import com.example.android.notesapplication.db.NotesDao

class NotesRepository {

    private val mNotesDao: NotesDao
    private val db: AppDatabase

    constructor(application: Application) {
        db = AppDatabase.getInstance(application)
        mNotesDao = db.notesDao()
    }

    fun getAllNotes(): LiveData<List<Notes>> {
        return mNotesDao.getAllNotes()
    }

    fun getFavNotes(): LiveData<List<Notes>> {
        return mNotesDao.getFavNotes()
    }

    fun insertNotes(notes: Notes) {
        mNotesDao.insertNotes(notes)
    }

    fun updateNotes(notes: Notes) {
        mNotesDao.updateNotes(notes)
    }

}